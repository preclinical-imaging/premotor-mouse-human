import nibabel as nib
import numpy as np
import pandas as pd
from scipy.cluster.hierarchy import dendrogram

def plot_dendrogram(model, **kwargs):
    # Create linkage matrix and then plot the dendrogram
    link2label = {}
    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
                link2label[i] = model.labels_[child_idx]
            else:
                current_count += counts[child_idx - n_samples]
                link2label[i] = link2label[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack(
        [model.children_, model.distances_, counts]
    ).astype(float)

    # Plot the corresponding dendrogram
    t = dendrogram(linkage_matrix, **kwargs)

    return linkage_matrix, t, link2label

def findVoxel(children, id_, maxval, level):
    if level > 10: # to avoid infinite recursion
        return -1
    coors = np.where(children==id_)
    tmp_val = children[coors[0], coors[1]]
    if tmp_val > maxval:
        return findVoxel(children, tmp_val-maxval, maxval, level+1)
    return tmp_val
