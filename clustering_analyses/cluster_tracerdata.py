import nibabel as nib
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
import os
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from utils import plot_dendrogram, findVoxel
from sklearn.cluster import AgglomerativeClustering

#########################################
#### Step 1) Read and preprocess the data
#########################################

# Brain mask to remove non-brain tissue
BrainPath = "/media/valverde/Oxford/ETH_Data/Template/QBI_atlas_2mm_v6_brainmask_filled.nii.gz"
# Tracer data. Shape = (51, 38, 98, 499) (last dim: # of subjects)
tracerdataPath = "/media/valverde/Oxford/tracer_data/tracer_merge_QBI.nii.gz"
# Mask of the motorcortex area (M1+M2)
M1M2Path = "/media/valverde/Oxford/Dorr_2008_Steadman_2013_Ullmann_2013_Richards_2011_Qiu_2016_Egan_2015_40micron/nifti/DSURQE_40micron_labels_M1M2_QBI_bin.nii.gz"

print("Reading and processing the data...")
# Tracer data
tracer = nib.load(tracerdataPath).get_fdata()
# Brain mask
brain = nib.load(BrainPath).get_fdata()
# M1M2 mask
M1M2im = nib.load(M1M2Path)
M1M2 = M1M2im.get_fdata() * brain

# Select the tracer data within the motorcortex area
data = tracer[M1M2==1]
print(f"Voxels of interest: {data.shape[0]}. Number of brains: {data.shape[1]}")

# Normalize the features between 0 and 1
data_norm = (data - data.min(axis=0)) / (data.max(axis=0) - data.min(axis=0) + 1e-15)

# Discard those experiments with most (80%) of the tracer data values equal to zero
ratio_nonzerovoxels_per_brain = (data!=0).sum(axis=0) / data.shape[0]
idx_brains = np.where( (ratio_nonzerovoxels_per_brain >= 0.2) * (ratio_nonzerovoxels_per_brain <= 1) )[0]

# Compute the correlation matrix
df = pd.DataFrame(data_norm[:, idx_brains].T)
corrmat = np.array(df.corr())
print(f"Dimensions of the correlation matrix: {corrmat.shape}")

######################################################
#### Step 2) Cluster the data and save the segmetation
######################################################
print("Running KMeans...")
# Set random_state=42 to get the same label IDs in the same areas
clusterer = KMeans(n_clusters=4, n_init=10, random_state=42).fit(corrmat)
segmentation = clusterer.labels_+1

# Save the segmentation in a new file
outputPath = "output/clustered_k4.nii.gz"

M1M2[M1M2==1] = segmentation

# Save segmentation
nib.save(nib.Nifti1Image(M1M2, affine=M1M2im.affine,
    header=M1M2im.header), outputPath)

#######################################
#### Step 3) Compute PCA and tSNE plots
#######################################

print("Computing PCA and tSNE plots")
text_legend = {}
text_legend[1] = "pM2"
text_legend[2] = "M1-RFA"
text_legend[4] = "aM2"
text_legend[3] = "M1-CFA"
order = [2, 3, 4, 1]
colors = {
        1: "#70AD47", # green
        2: "#FFDA6E", # orange
        3: "#FF0101", # red
        4: "#547EC9", # blue
        }

reduced_components_pca = PCA(n_components=2).fit_transform(corrmat)
reduced_components_tsne = TSNE(n_components=2, random_state=42).fit_transform(corrmat)

components = [reduced_components_pca, reduced_components_tsne]
figtitle = ["PCA", "t-SNE"]

for features, title in zip(components, figtitle):
    # PLOT
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 5))
    for i in order:
        ax.scatter(features[segmentation==i, 0],
                   features[segmentation==i, 1],
                   label=text_legend[i],
                   color=colors[i])
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')
    ax.set_yticklabels([""])
    ax.set_xticklabels([""])
    ax.set_xlabel("First component", fontsize=18)
    ax.set_ylabel("Second component", fontsize=18)
    ax.set_title(f"{title} (thr=80%, k=4)", fontsize=20)
    ax.legend()
    plt.savefig(os.path.join("output", f"{title.lower()}_k4.png"),
                dpi=150)

############################
#### Step 4) Plot dendrogram
############################
print("Computing the dendrogram")
colors = {
        1: "#FFDA6E", # orange
        2: "#70AD47", # green
        3: "#FF0101", # red
        4: "#547EC9", # blue
        }
levels = 6
model = AgglomerativeClustering(n_clusters=4, linkage="ward", compute_distances=True)
model = model.fit(corrmat)
linkage_matrix, R, l2l = plot_dendrogram(model, truncate_mode="level", p=levels, no_plot=True)

# create a label dictionary
labels = {}
for l in R["leaves"]:
    vox = findVoxel(model.children_, l, maxval=899, level=1)[0]
    labels[l] = l2l[vox]
def llf(xx):
    return ""
default_color = "#000000"
def lcf(xx):
    vox = findVoxel(model.children_, xx, maxval=899, level=1)
    if len(vox) == 0:
        return default_color
    if vox > 894:
        return default_color
    lab = l2l[vox[0]]
    return colors[lab+1]

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 5))
plt.title("Hierarchical Clustering Dendrogram", fontsize=20)
# plot the top three levels of the dendrogram
linkage_matrix, t, _ = plot_dendrogram(model, truncate_mode="level",
        p=levels, leaf_label_func=llf, link_color_func=lcf)
        # For displaying the number of leaves in each node.
plt.ylabel("Distance", fontsize=18)
ax.set_yticklabels([0, 25, 50, 75, 100, 125, 150, 175], fontsize=16)

plt.savefig(os.path.join("output", "dendrogram_k4.png"), dpi=150)
print("End")
