#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch
scriptDir=/vols/Scratch/comparative_scripts
mkdir -p $workDir/connectivity_maps_mice

subjList="001
002
003
004
005
006
007
008
009
010
011
012
013
014
015
016
017
018
019
020"

seedList=""

template=/vols/Scratch/Valerio_data/Template/QBI_atlas_2mm_v6.nii

for subj in $subjList; do
  echo "SBCA for $subj"

  for seed in $seedList; do
  echo "$seed"

  fsl_sbca -i $workDir/Valerio_data/Data/"$subj"_FIXed_QBI.nii.gz -s $template -t $scriptDir/m2_homologies/seeds/"$seed"_mouse_seed.nii.gz -o $workDir/connectivity_maps_mice/"$subj"_"$seed"

  done

done

echo "done"
