#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch
scriptDir=/vols/Scratch/comparative_scripts
mkdir -p $workDir/connectivity_maps_tracing

seedList="M1 ALM aM2 pM2"

template=$scriptDir/registrations/QBI_atlas_2mm_v6.nii

  for seed in $seedList; do
  echo "$seed"

  fsl_sbca -i $scriptDir/registrations/tracer_merge_QBI -s $template -t $scriptDir/m2_homologies/seeds/parcellation_DSURQE_"$seed".nii.gz -o $workDir/connectivity_maps_tracing/DSURQE_"$seed"_tracing

done

echo "done"
