# Project Repository for "The mouse motor system contains multiple premotor areas and partially follows human organisational principles"

Welcome to the project repository for the materials and code accompanying _"The mouse motor system contains multiple premotor areas and partially follows human
organisational principles"_
 by Lazari and colleagues!

We hope you find this useful, and you are welcome to get in touch (alberto.lazari@ndcn.ox.ac.uk) if more information is needed.
