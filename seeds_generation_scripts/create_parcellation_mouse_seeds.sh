#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/comparative_scripts/m2_homologies/parcellations

parcellation=$workDir/tracer_parcellation/parcellation_3.nii.gz

# 1 is pM2, 2 is M1, 3 is aM2, 4 is ALM
fslmaths $parcellation -uthr 1 -thr 1 -bin $workDir/seeds/pM2.nii.gz
fslmaths $parcellation -uthr 3 -thr 3 -bin $workDir/seeds/aM2.nii.gz
fslmaths $parcellation -uthr 2 -thr 2 -bin $workDir/seeds/M1.nii.gz
fslmaths $parcellation -uthr 4 -thr 4 -bin $workDir/seeds/ALM.nii.gz
