#!/usr/bin/env bash

# definitions
scriptDir=/vols/Scratch/comparative_scripts/m2_homologies
workDir=$scriptDir/seeds
mkdir -p $scriptDir/seeds

refImg=/vols/Scratch/Valerio_data/Template/QBI_atlas100_AP_swap.nii.gz

ROIlist="S1Left S2Left M1Left M2posterior M2anterior A1 V1 IL PL CG12 Retrosplenial Orbitofrontal BLA VentralHippo DorsalHippo M1 S1 S2 TeA"

# create ROIs out of Paxinos-based points

for roi in $ROIlist; do

echo $roi
fslmaths $scriptDir/seeds/"$roi"_mouse_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/"$roi"_mouse_seed -odt float

done
