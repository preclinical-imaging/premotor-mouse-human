#!/usr/bin/env bash

# definitions
scriptDir=/vols/Scratch/comparative_scripts/m2_homologies
workDir=$scriptDir/seeds
mkdir -p $scriptDir/seeds

refImg=$FSLDIR/data/standard/MNI152_T1_2mm_brain

# M1 - Primary Motor Cortex
coord=($(echo 40 -20 58 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/M1_human_point
fslmaths $scriptDir/seeds/M1_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/M1_human_seed -odt float

# FEF
coord=($(echo 46 2 35 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/FEF_human_point
fslmaths $scriptDir/seeds/FEF_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/FEF_human_seed -odt float

# 6ma
coord=($(echo 17 2 70 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/6ma_human_point
fslmaths $scriptDir/seeds/6ma_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/6ma_human_seed -odt float

# SFL
coord=($(echo 8 17 65 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/SFL_human_point
fslmaths $scriptDir/seeds/SFL_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/SFL_human_seed -odt float

# PMv
coord=($(echo 61 7 28 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/PMv_human_point
fslmaths $scriptDir/seeds/PMv_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/PMv_human_seed -odt float

# PMd
coord=($(echo 35 -11 65 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/PMd_human_point
fslmaths $scriptDir/seeds/PMd_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/PMd_human_seed -odt float

# Left M1 - Primary Motor Cortex
coord=($(echo -40 -20 58 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/M1Left_human_point
fslmaths $scriptDir/seeds/M1Left_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/M1Left_human_seed -odt float

# Area 25
coord=($(echo 4	14 -10 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/Area25_human_point
fslmaths $scriptDir/seeds/Area25_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/Area25_human_seed -odt float

# Area 32pl
coord=($(echo 2	40 2 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/Area32pl_human_point
fslmaths $scriptDir/seeds/Area32pl_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/Area32pl_human_seed -odt float

# Area 24
coord=($(echo 2	18 28 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/Area24_human_point
fslmaths $scriptDir/seeds/Area24_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/Area24_human_seed -odt float

# Retrosplenial cortex
coord=($(echo 2	-44	22 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/Retrosplenial_human_point
fslmaths $scriptDir/seeds/Retrosplenial_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/Retrosplenial_human_seed -odt float

# Area 13
coord=($(echo 18 24 -22 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/Area13_human_point
fslmaths $scriptDir/seeds/Area13_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/Area13_human_seed -odt float

# Basolateral Amygdala
coord=($(echo 26 -2 -18 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/BasoAmyg_human_point
fslmaths $scriptDir/seeds/BasoAmyg_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/BasoAmyg_human_seed -odt float

# Anterior Hippocampus
coord=($(echo 26 -16 -20 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/AntHippo_human_point
fslmaths $scriptDir/seeds/AntHippo_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/AntHippo_human_seed -odt float

# Posterior Hippocampus
coord=($(echo 32 -34 -6 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/PostHippo_human_point
fslmaths $scriptDir/seeds/PostHippo_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/PostHippo_human_seed -odt float

# S1
coord=($(echo 44 -32 58 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/S1_human_point
fslmaths $scriptDir/seeds/S1_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/S1_human_seed -odt float

# S2 (OP1)
coord=($(echo 56 -22 20 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/S2_human_point
fslmaths $scriptDir/seeds/S2_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/S2_human_seed -odt float

# TPJp
coord=($(echo 54 -60 28 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/TPJp_human_point
fslmaths $scriptDir/seeds/TPJp_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/TPJp_human_seed -odt float

# A1
coord=($(echo 41 -23 13 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/A1_human_point
fslmaths $scriptDir/seeds/A1_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/A1_human_seed -odt float

# V1
coord=($(echo 9 -86 4 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/V1_human_point
fslmaths $scriptDir/seeds/V1_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/V1_human_seed -odt float

# Left S1
coord=($(echo -44 -32 58 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/S1Left_human_point
fslmaths $scriptDir/seeds/S1Left_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/S1Left_human_seed -odt float

# Left S2 (OP1)
coord=($(echo -56 -22 20 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/S2Left_human_point
fslmaths $scriptDir/seeds/S2Left_human_point -kernel boxv 3 -fmean -bin $scriptDir/seeds/S2Left_human_seed -odt float
