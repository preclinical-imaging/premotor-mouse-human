#!/usr/bin/env bash

# definitions
origDir=/vols/Scratch/preclinicalMPM_scripts
workDir=/vols/Scratch/comparative_scripts/mouse_MPM_results

maptype="MT R1 R2"

template=/vols/Scratch/comparative_scripts/registrations/QBI_atlas_2mm_v6.nii

for map in $maptype; do
echo "$map"

fslmerge -t $workDir/all_"$map" $workDir/"$map"/*_"$map"_registered.nii.gz

done

echo "done"
