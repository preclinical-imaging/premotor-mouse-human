#!/usr/bin/env bash

# definitions
origDir=/vols/Scratch/MPM_rodent_data
workDir=/vols/Scratch/comparative_scripts/mouse_MPM_results

subjList="20210517_175914_AL2_MPM_1_1
20210604_181641_AL3_AL3_MPM_3_1_3
20210609_200948_AL5_MPM_1_3
20210607_192507_AL6_MPM_4_1_6
20210611_205425_AL7_MPM_2_1_3
20210402_231711_AL8_AL8_MPM_1_3
20210603_165354_AL11_AL11_MPM_2_1_3
20210617_183610_AL13_MPM_1_3
20210614_185959_AL15_MPM_1_2
20210616_175000_AL16_MPM_2_1_2
20210608_164021_AL18_MPM_1_3
20210618_202710_AL19_MPM_1_1"
maptype="MT R1 R2"

for subj in $subjList; do

echo "$subj"

# make a copy of MPM map
cp $origDir/"$subj"_NIFTI/SubjectDIR_RepetitionAverage/hMRI_Results/Results/PDW_echo_mean_1_MT.nii $workDir/MT/"$subj"_MT.nii
cp $origDir/"$subj"_NIFTI/SubjectDIR_RepetitionAverage/hMRI_Results/Results/PDW_echo_mean_1_R1.nii $workDir/R1/"$subj"_R1.nii
cp $origDir/"$subj"_NIFTI/SubjectDIR_RepetitionAverage/hMRI_Results/Results/PDW_echo_mean_1_R2s_OLS.nii $workDir/R2/"$subj"_R2.nii

done

echo "done"
