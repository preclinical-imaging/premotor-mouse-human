#!/usr/bin/env bash

# definitions
origDir=/vols/Scratch/preclinicalMPM_scripts
workDir=/vols/Scratch/comparative_scripts/mouse_MPM_results

subjList="20210517_175914_AL2_MPM_1_1
20210604_181641_AL3_AL3_MPM_3_1_3
20210609_200948_AL5_MPM_1_3
20210607_192507_AL6_MPM_4_1_6
20210611_205425_AL7_MPM_2_1_3
20210402_231711_AL8_AL8_MPM_1_3
20210603_165354_AL11_AL11_MPM_2_1_3
20210617_183610_AL13_MPM_1_3
20210614_185959_AL15_MPM_1_2
20210616_175000_AL16_MPM_2_1_2
20210608_164021_AL18_MPM_1_3
20210618_202710_AL19_MPM_1_1"

templateDir=/vols/Scratch/comparative_scripts/registrations/
template=$templateDir/QBI_atlas_2mm_v6.nii
templateMask=$templateDir/QBI_atlas_2mm_v6_brainmask_filled.nii.gz

for subj in $subjList; do
echo "$subj"

  fslchpixdim $workDir/MT/"$subj"_MT.nii.gz 1 1 1
  flirt -in $workDir/MT/"$subj"_MT.nii.gz -ref $template -out $workDir/MT/"$subj"_MT_registered.nii -omat $workDir/MT/"$subj"_MT_registered.mat -dof 7 -searchry -180 180

  convert_xfm -omat $workDir/MT/"$subj"_MT_registered_template2mpm.mat -inverse $workDir/MT/"$subj"_MT_registered.mat

  flirt -in $templateMask -ref $workDir/MT/"$subj"_MT.nii.gz -applyxfm -init $workDir/MT/"$subj"_MT_registered_template2mpm.mat -interp nearestneighbour -out $workDir/MT/"$subj"_MT_brainmask.nii.gz

  fslmaths $workDir/MT/"$subj"_MT.nii.gz -mas $workDir/MT/"$subj"_MT_brainmask.nii.gz $workDir/MT/"$subj"_MT_brain.nii.gz

  flirt -in $workDir/MT/"$subj"_MT_brain.nii.gz -ref $template -out $workDir/MT/"$subj"_MT_registered.nii -omat $workDir/MT/"$subj"_MT_registered.mat -dof 12 -searchry -180 180

  # now apply the transform to R1 and R2
  fslchpixdim $workDir/R1/"$subj"_R1.nii.gz 1 1 1
  flirt -in $workDir/R1/"$subj"_R1.nii.gz -ref $template -applyxfm -init $workDir/MT/"$subj"_MT_registered.mat -out $workDir/R1/"$subj"_R1_registered.nii
  fslchpixdim $workDir/R2/"$subj"_R2.nii.gz 1 1 1
  flirt -in $workDir/R2/"$subj"_R2.nii.gz -ref $template -applyxfm -init $workDir/MT/"$subj"_MT_registered.mat -out $workDir/R2/"$subj"_R2_registered.nii

echo "done"

done
